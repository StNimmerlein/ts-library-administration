# Extreme Typisierung in TypeScript - Was ist alles möglich und ist das auch praktisch einsetzbar?

## Vorschau

Das Typsystem in TypeScript ist sehr vielseitig. Von einfachen Union Types bis hin zu komplexen rekursiven generischen Typen, erlaubt es, eigene Typen perfekt zugeschnitten auf den jeweiligen Anwendungsfall zu definieren.
Doch wie weit kann man das Ganze treiben? Welche Vorteile bringt das - und welche Hürden?

In diesem Artikel werden wir Typen für ein gegebenes Szenario definieren und immer weiter spezialisieren, um die Möglichkeiten und Grenzen auszutesten.

## *Disclaimer*

1. In diesem Artikel geht es um Typen in TypeScript. Durch *Literal Types* kann man manchmal den Eindruck gewinnen, dass man sich gerade über konkrete Werte unterhält. Daher sollte man immer im Hinterkopf behalten, dass hier in dem Artikel mit `'a'` in der Regel der Typ gemeint ist, der ausschließlich den Wert `'a'` zulässt, und nicht der Wert `'a'` selbst.
2. Da TypeScript ein Superset von JavaScript ist, welches nur bis zur Compile-Zeit existiert und dort aus dem Code entfernt wird, müssen wir mit einigen Einschränkungen leben und auch durch den Code sicherstellen, dass die Typisierung eingehalten wird.
Wenn wir zum Beispiel eine Funktion `someFunc: () => 1 | 2` haben, welche nur eine `1` oder `2` zurückgeben soll, müssen wir dies auch durch den Code sicherstellen. Sollte sie in irgendeiner Situation doch mal eine `3` zurückgeben, würde dieser Fehler zur Laufzeit nicht auffallen (anders, als man es zum Beispiel von Java gewöhnt ist, wo dann eine Exception fliegt). Man muss also durch selbstgeschriebenen Code sicherstellen, dass dies nicht passiert.
Solange man sich im eigenen Kontext bewegt, alles streng typisiert ist und man keine ungeprüften Casts macht, kann man sich darauf verlassen, dass die Typen und Werte konsistent bleiben. Sobald man aber Daten aus anderen Quellen bezieht (Benutzereingaben, HTTP-Endpunkt, ...), müssen diese Quellen entweder zu 100 Prozent zuverlässig sein (die Typisierung betreffend) oder es muss durch Code geprüft werden, dass die Daten auch die Typisierung widerspiegeln.

### Beispiel

**Fehleranfällig:**
```typescript
function getInput(): 1 | 2 {
    return Number(someNumberInputField.value)
}
```

**Abgesichert:**
```typescript
function getInput(): 1 | 2 {
    const enteredNumber = Number(someNumberInputField.value)
    if (enteredNumber !== 1 && enteredNumber !== 2) {
        throw new Error('Input is neither 1 nor 2')
    }
    return enteredNumber
}
```

Im Folgenden beschäftigen wir uns aber nicht weiter mit der Validierung und gehen davon aus, dass wir uns bereits in unserem eigenen Safe Space bewegen. Das Schreiben der Prüfungen sollte aber auch in der Regel ziemlich leicht, lediglich etwas aufwendig, sein.

## Szenario

Wir stellen uns folgendes Szenario vor, für das wir eine Software schreiben möchten:

- Wir haben eine Bibliothek, welche wir verwalten.
- Die Bibliothek hat ein Inventar, bestehend aus Büchern und Filmen.
  - Bücher haben einen Namen, ID, Autor und Abteilung.
    - Die Abteilung besteht aus den ersten beiden Buchstaben des Autors in Großbuchstaben. Der Einfachheit halber nehmen wir an, dass jeder Autor aus mindestens zwei Zeichen besteht.
    - Die IDs für Bücher starten immer mit dem Buchstaben `B`, gefolgt von einem Bindestrich, der Abteilung, einem Bindestrich und einer Nummer.
  - Filme haben einen Namen, ID und Altersfreigabe (0, 6, 12, 16, 18).
    - Die IDs für Filme starten mit einem 'M', gefolgt von einem Bindestrich, der Altersfreigabe, einem Bindestrich und einer Nummer.

## Die Typen

### Erste Definitionen

Zunächst wollen wir die Typen so einfach wie möglich definieren, ohne uns besonders Gedanken zu machen:

```typescript
type Book = {
    name: string
    id: string
    author: string
    section: string
}

type Movie = {
    name: string
    id: string
    ageRating: number
}
```

Ok, wir können schon sehen, dass wir eine gemeinsame Oberklasse extrahieren können:

```typescript

type Media = {
    name: string
    id: string
}

type Book = Media & {
    author: string
    section: string
}

type Movie = Media & {
    ageRating: number
}
```

Damit sind unsere Typen fertig definiert. Jedenfalls wären sie es in einer Sprache wie Java, die nicht so ein flexibles Typsystem besitzt. Betrachten wir nun Stück für Stück, wie wir die Typen in TypeScript noch enger zurren können:

### Altersfreigabe

Wir wissen bereits, dass die Altersfreigabe lediglich die Werte 0, 6, 12, 16 und 18 annehmen kann. Der aktuelle Typ `number` erlaubt aber auch jede andere Zahl, sogar negative Zahlen.
Um das zu verhindern, können wir einen *Union Type* von *Literal Types* verwenden:

```typescript
type Movie = Media & {
    ageRating: 0 | 6 | 12 | 16 | 18
}
```

Sieht schon besser aus. Wenn wir den Typ für die Altersempfehlung noch als eigenständigen Typen extrahieren, können wir ihn sogar wiederverwenden und durch die Benennung mit einer Semantik belegen:

```typescript
type AgeRating = 0 | 6 | 12 | 16 | 18

type Movie = Media & {
    ageRating: AgeRating
}
```

### ID

Betrachten wir nun die ID. Wir kennen die Struktur der IDs für Filme und Bücher. Also können wir auch hier die Typen etwas strenger fassen und sogar unseren Typen für die Altersempfehlung wiederverwenden.
Da wir die ID momentan im gemeinsamen Supertyp haben, lassen wir sie dort zunächst allgemein und spezifizieren sie in den Subtypen genauer. Da die spezielleren Typen der IDs der Bücher und Filme weiterhin Subtypen von `string` bleiben werden, ist das auch völlig legitim.
Zum Konkretisieren der ID-Typen können wir *Template Literal Types* verwenden:

```typescript
type Media = {
    name: string
    id: string
}

type Book = {
    author: string
    section: string
    id: `B-${string}-${number}`
}

type Movie = {
    ageRating: AgeRating
    id: `M-${AgeRating}-${number}`
}
```

Wenn wir nun versuchen, einem Buch eine nicht passende ID `book.id = '12345'` zuzuweisen, wird TypeScript diese Stelle im Code als Fehler markieren und beim Kompilieren darauf hinweisen. `book.id = 'B-SomeSection-12345'` funktioniert hingegen weiterhin.
Für Filme haben wir den `AgeRating`-Typ wiederverwenden können, sodass wir zwar zum Beispiel die Werte `'M-0-12345'` und `'M-18-4'` zuweisen können, nicht jedoch
```typescript
movie.id = '0-12345' // Das `M-` am Anfang fehlt
movie.id = 'M-12345' // Der `-${AgeRating}`-Teil fehlt
movie.id = 'M-5-12345' // 5 ist keine gültige Altersempfehlung
movie.id = 'M-6-12xy5' // 12xy5 ist keine Zahl
```

Da wir auch diese ID-Typen sicherlich woanders nochmal gebrauchen können, extrahieren wir sie wieder:

```typescript
type BookId = `B-${string}-${number}`
type MovieId = `M-${AgeRating}-${number}`
```

Wir können noch einen Schritt weiter gehen und die Werte für den Buch- und Filmpräfix extrahieren, falls gewünscht. Das bringt uns in diesem Fall keinen direkten Vorteil, liefert aber wieder eine gewisse Semantik.
Außerdem können wir, da wir wissen, dass Medias nur Bücher und Filme sein können, auch eine `MediaId` definieren.

```typescript
type BookType = 'B'
type MovieType = 'M'
type BookId = `${BookType}-${string}-${number}`
type MovieId = `${MovieType}-${AgeRating}-${number}`
type MediaId = BookId | MovieId

type Media = {
    id: MediaId
    name: string
}

type Book = Media & {
    id: BookId
    author: string
    section: string
}

type Movie = Media & {
    id: MovieId
    ageRating: AgeRating
}
```

### Abteilung

Werfen wir nun einen Blick auf die Abteilung. Können wir auch hier einen passenderen Typen wählen? Wir wissen, dass sie aus exakt zwei Großbuchstaben besteht.
TypeScript bietet den *Utility Type* `Uppercase<T>` an, der perfekt dafür geeignet scheint. Allerdings funktioniert er nur auf Literal Types und `Uppercase<string>` erlaubt weiterhin Kleinbuchstaben.
Wir brauchen also einen Workaround. Wir könnten einen Typ `type Letter = 'a' | 'b' | 'c' | ...` definieren, der alle Zeichen zulässt, die in einem Namen vorkommen können. Anschließend könnten wir dann ``type Section = Uppercase<`${Letter}${Letter}`>`` definieren.
Alternativ könnten wir einen vom Autor abhängigen generischen Typ `type Section<AUTHOR extends string>` anlegen, der den Autor direkt miteinbezieht.
Betrachten wir beide Herangehensweisen.

#### Letter

`type Letter = 'a' | 'b' | ...` zu tippen ist ziemlich lästig. Es wäre netter, wenn wir uns wenigstens die ganzen Anführungszeichen und `|` sparen könnten. Also schreiben wir zuerst einen Hilfstypen. Dazu benutzen wir *Generics* und bauen einen rekursiven *Conditional Type*:

```typescript
type CharacterOf<CHARACTERS extends string> = CHARACTERS extends `${infer HEAD}${infer TAIL}`
    ? HEAD | CharacterOf<TAIL>
    : never
```

Ok, das sieht schon etwas komplizierter aus als die Typen im ersten Teil. Brechen wir den Typ in einzelne Teile herunter, um zu verstehen, was hier passiert:

```
type CharacterOf<CHARACTERS extends string>
```

Wir definieren einen generischen Typen, der einen String-Typ als Parameter `CHARACTERS` annimmt.

```
CHARACTERS extends `${infer HEAD}${infer TAIL}`
```

Dann prüfen wir, ob der gegebene Parameter `CHARACTERS` als Kombination `${HEAD}${TAIL}` von zwei anderen abgeleiteten String-Typen `HEAD` und `TAIL` repräsentiert werden kann.
Wenn `CHARACTERS` einfach `string` ist, dann ist dieser Ausdruck falsch (könnte ja zum Beispiel der leere String sein). Ist `CHARACTERS` aber ein Literal String Type, dann wird die Sache interessant:
Ist `CHARACTERS` der Literal Type des leeren Strings, `''`, so evaluiert der Ausdruck zu false (theoretisch entspricht der leere String `''` natürlich einer Konkatenation zweier weiterer leerer Strings `${''}${''}`, aber hier zieht TypeScript schon eine sinnvolle Grenze).
Ist `CHARACTERS` ein anderer Literal String Type, zum Beispiel `abcde`, dann kann er auch dargestellt werden als `${'a'}${'bcde'}` (oder auch `${'ab'}${'cde'}` und so weiter. TypeScript matcht den linken Teil aber nur so weit wie nötig und wählt daher die erste Variante).
Der Ausdruck evaluiert also zu `true` und in diesem Fall sind die Typen `HEAD` = `'a'` und `TAIL` = `'bcde'` abgeleitet worden und verfügbar.
(Ist `CHARACTERS` ein literal string type mit einer Länge von eins, zum Beispiel `'a'`, dann evaluiert der Ausdruck auch zu `true`, denn `'a'` kann mit `HEAD = 'a'` und `TAIL = ''` dargestellt werden).

```
? HEAD | CharacterOf<TAIL>
```

Wenn der vorige Ausdruck als wahr evaluierte, dann haben wir nun in `HEAD` das erste Zeichen von `CHARACTERS` und in `TAIL` den Rest. Alle möglichen Zeichen von `CHARACTERS` können also dargestellt werden als [`HEAD` *oder* [Alle möglichen Zeichen von `TAIL`]].
Das ist eine Rekursion, wie sie im Buche steht und wir geben somit als "Ergebnis" `HEAD | CharactersOf<TAIL>` zurück.

```
: never
```

Für die Fälle `CHARACTERS = string` und `CHARACTERS = ''` laufen wir in den `else`-Fall. Der erste davon interessiert uns nicht weiter. Für den Fall, dass `CHARACTERS = ''` ist und wir alle Zeichen zulassen wollen,
die im gegebenen String vorkommen, so dürfen wir überhaupt nichts zulassen (den leeren String wollen wir explizit ausschließen). Somit geben wir den Typen `never` zurück, da wir nie einen gültigen Wert für diesen Typen finden werden.
Never fügt sich außerdem wunderbar als Schluss in unsere Rekursion ein, denn `'a' | 'b' | never` entspricht einfach dem Typen `'a' | 'b'`.

Da eine Rekursion immer eine Abbruchbedingung benötigt, wollen wir dies nun kurz überprüfen.
In jeder Iteration wird `TAIL` ein Element kürzer. Hat es eine Länge von 0 erreicht, ist also der leere String, dann bricht die Rekursion ab, wie wir oben gesehen haben.

Wie sähe nun also eine Implementierung mit diesem neuen Typen aus? (In diesem Beispiel lassen wir nur die Buchstaben a-z für Autoren zu. Andere Zeichen können aber nach Belieben hinzugefügt werden, indem der "String" (eigentlich der Literal Type) in Zeile 4 erweitert wird).

```typescript
type CharachterOf<T extends string> = T extends `${infer HEAD}${infer TAIL}`
    ? HEAD | CharachterOf<TAIL>
    : never
type Letter = CharachterOf<'abcdefghijklmnopqrstuvwxyz'>
type Section = Uppercase<`${Letter}${Letter}`>
type BookId = `${BookType}-${Section}-${number}`

type Book = Media & {
    id: BookId
    author: string
    section: Section
}
```

#### Generisches Book

Die zweite Lösung, die wir uns anschauen wollen, ist es, `Section` generisch und vom Autor abhängig zu machen. Dazu brauchen wir dann auch für jeden Autor einen Literal Type und können diesen benutzen, um den Typen für die Abteilung weiter einzuschränken:

```typescript
type Section<AUTHOR extends string> = AUTHOR extends `${infer FIRST}${infer SECOND}${infer TAIL}`
    ? Uppercase<`${FIRST}${SECOND}`>
    : string
```

Falls `AUTHOR` aus mindestens zwei Zeichen besteht, nehmen wir uns diese beiden und geben sie in Großbuchstaben zurück. Andernfalls geben wir einfach `string` zurück; dieser Fall interessiert uns hier aber nicht weiter, wir hatten ihn in den Annahmen ja explizit ausgeschlossen.
Der Conditional Type ist dennoch notwendig, um an die abgeleiteten Typen heranzukommen.

Konsequenterweise müssen wir nun auch `Book` generisch machen. Dadurch können wir aber auch den Typen für die ID noch etwas anpassen:

```typescript
type Section<AUTHOR extends string> = AUTHOR extends `${infer FIRST}${infer SECOND}${infer TAIL}`
    ? Uppercase<`${FIRST}${SECOND}`>
    : string
type BookId<AUTHOR extends string> = `${BookType}-${Section<AUTHOR>}-${number}`


type Book<AUTHOR extends string> = Media & {
    id: BookId<AUTHOR>
    author: AUTHOR
    section: Section<AUTHOR>
}
```

#### Vergleich der Varianten

Wir haben zwei Möglichkeiten gesehen, um den Typen für die Abteilung noch weiter einzuschränken (und sicherlich gibt es noch viele weitere). Betrachten wir nun Vor- und Nachteile der beiden vorgestellten Varianten.

**Lösung mit `Letter`**

*Vorteile*
- Der Typ `Section` ist immer noch recht allgemein. Wir können ihn also leichter auch an anderen Stellen wiederverwenden; beispielsweise, wenn wir auch für Filme eine Abteilung einführen würden.
- Falls wir uns dazu entscheiden, den Algorithmus zur Generierung der Abteilung zu ändern (zum Beispiel "Erster Buchstabe des Vornamens, dann erster Buchstabe des Nachnamens"), können wir den Typen ohne Veränderung weiterverwenden.
- Er ist restriktiv genug, damit wir uns drauf verlassen können, dass eine Abteilung immer nur aus zwei Großbuchstaben besteht. Außerdem erzwingt er dadurch auch in der Programmierung, darauf zu achten, dass die Sektionen syntaktisch richtig generiert werden.

*Nachteile*
- Es ist weiterhin möglich, ein Buch `{author: 'Twain, Mark', section: 'AB', id: 'B-XY-12345'}` zu haben, welches in unserem Typsystem völlig korrekt vom Typ `Book` ist.
- Es kann sehr aufwendig sein, alle möglichen Zeichen aufzulisten, die in Autorennamen vorkommen könnten.
- Wenn der Name eines Autors ein Zeichen beinhaltet, an das wir nicht gedacht haben, ist es unmöglich, einen gültigen Bucheintrag dafür anzulegen.

**Lösung mit generischem `Book` und `Section`**

*Vorteile*
- Der Typ ist so strikt wie es nur geht. Es ist nicht möglich, ein in unserem Typsystem gültiges Buch  `{author: 'Twain, Mark', section: 'AB', id: 'B-XY-12345'}` zu haben.
- Die Autovervollständigung der IDE kann beim Setzen von Werten optimal unterstützen.

*Nachteile*
- Wenn man den Typ für eine `Book`-Variable spezifizieren möchte, muss man immer schon den Autor kennen und, mehr noch, einen eigenen Literal Type für ihn haben. In fast allen Fällen ist das nicht möglich; einen sinnvollen Workaround gibt es auch nicht. Eine Methode, die zum Beispiel ein Buch über einen HttpRequest abruft, wird so gut wie nie `Book<SomethingSpecific>` zurückgeben können sondern lediglich `Book<string>`.
- Wenn wir einfach nur ein Buch haben und uns nicht um den Autor kümmern wollen, also einfach `Book<string>` verwenden, ist der daraus resultierende Type `Section<string>` weniger restriktiv als der in der ersten Variante.

#### Zusammenfassung

In fast allen Anwendungen kommen die Daten durch eine externe Schnittstelle (Datenbank, Benutzereingabe, Http Request, ...). In all diesen Fällen werden wir `Book<AUTHOR>` nicht sinnvoll anwenden können. Diese Lösung bietet nur in Anwendungen Vorteile, die sämtliche Daten selbst erzeugt oder die zum Beispiel für jeden möglichen Autor auch einen eigenen Typen besitzt, um dann an den Schnittstellen und mit Type Guards genau zu entscheiden, womit man es zu tun hat. Für den normalen Alltag ist das somit keine sinnvolle Lösung.
Auch wenn die Lösung mit `Letter` also auf dem Papier weniger restriktiv erscheint, so bietet sie in der Realität doch die größere Sicherheit.

Natürlich ist auch die Lösung mit `Letter` nicht optimal; insbesondere, dass jedes einzelne Zeichen aufgelistet werden muss, ist fast schon ein Deal Breaker. Hier wurden mit Absicht die Typen extrem verschärft, um die Möglichkeiten und Grenzen auszuloten.

## Warum sollten wir überhaupt solche spezifischen Typen verwenden?

Gegenfrage: Warum sollen wir überhaupt Typen verwenden?
Ganz offensichtlich bieten uns Typen wie "string" und "number" Vorteile gegenüber "any".
Und wenn wir schon die Möglichkeiten für spezifischere Typen haben, warum sollte man dann schon bei "string" aufhören?
Jede Strukturinformation, die wir kennen und durch Typen festzurren können, kann helfen, Fehler zu verringern. Sie kann den Code besser les- und wartbar machen.
Solange man sich im sicheren Umfeld seiner Typisierung bewegt, kann es sogar helfen, den Testaufwand zu reduzieren, da die Ergebnisse bereits durch die Typisierung sichergestellt sind.

Betrachten wir ein paar kleine Codebeispiele, welche die strenge Typisierung (mit der `Letter`-Variante) nutzen. Anschließend überlegen wir, vor welchen Fehlern uns die spezifischeren Typen bewahren.

### Eine Abteilung für einen gegebenen Autoren bestimmen

```typescript
function getSectionForAuthor(author: string): Section {
    return author.substring(0,2).toUpperCase() as Section
}
```

Typescript kann leider nicht selbstständig ableiten, dass der Rückgabetyp dieses Ausdrucks `Section` ist, weswegen wir casten müssen.
Tun wir dies nicht, so wirft Typescript einen Fehler, dass ein `string` nicht einer `Section` zugewiesen werden kann. Inwiefern hilft uns hier also der `Section`-Typ, wenn wir doch sowieso casten müssen?
Schreibt man einen cast, so sollte man an dieser Stelle besondere Aufmerksamkeit darauf lenken, ob der Cast so auch zulässig ist. Dadurch ist man gezwungen, nochmal genauer über den Code nachzudenken und bemerkt Fehler möglicherweise eher.
Zugegeben, das ist an dieser Stelle nur ein kleiner Vorteil, aber Kleinvieh macht bekanntlich auch Mist.

### Mögliche Altersbeschränkungen erhalten (zum Beispiel für ein select)

```typescript
function getPossibleAgeRatings(): AgeRating[] {
    return [0, 6, 12, 16, 18]
}
```

An dieser Stelle bewahrt uns der Typ vor Tippfehlern oder falschen Annahmen (zum Beispiel, dass es eine Altersbeschränkung ab 17 gäbe).
Falls im zurückgegebenen Array eine ungültige Zahl vorkommt, so wirft Typescript bereits beim Kompilieren einen Fehler.

Außerdem kann man hier auch auf die Autovervollständigung der IDE zurückgreifen (je nach IDE). Diese erkennt, dass ein `AgeRating`-Array zurückgegeben werden soll und schlägt daher beim Tippen der Arrayeinträge die möglichen Werte von `AgeRating` vor.

Für einen noch besseren Typen zu diesem Beispiele siehe den Bonus am Ende des Artikels. 


### Ein neues Buch anlegen

Nehmen wir an, dass wir bereits folgende Funktionen haben, die uns eine gültige Nummer und Abteilung liefern:
```typescript
declare function getNextNumber(): number
declare function getSectionForAuthor(author: string): Section
```

```typescript
function createBook(name: string, author: string): Book {
    const section = getSectionForAuthor(author)
    return {
        name,
        author,
        section,
        id: `B-${section}-${getNextNumber()}` as const
    }
}
```

(Hier müssen wir `as const` verwenden, um Typescript zu instruieren, den strengstmöglichen Typen für diese Variable abzuleiten.)

Auch hier schützt uns die Typisierung wieder vor Tippfehlern, denn sie erzwingt, dass die ID mit einem `B` beginnt und auch die Bindestriche enthält. Ebenso ist die Reihenfolge der einzelnen Abschnitte in der ID so festgelegt.

## Zusammenfassung

Natürlich lassen sich die obigen Beispiele auch einfach durch Unittests abdecken (und das sollten sie auch weiterhin).
Nichtsdestotrotz erhält man bereits beim Implementieren etwas mehr Sicherheit, deutlich bessere IDE-Unterstützung und vor allem ist der Code
aufgeräumter und besser verständlich (wenn man von den Typdefinitionen vielleicht einmal absieht, die teilweise wirklich etwas abenteuerlich sein können).
Eine `AgeRating` beinhaltet einfach viel mehr Semantik als ein `number`.
Es lohnt sich also, etwas mehr Bedacht in die Typen zu stecken und die Möglichkeiten von TypeScript auch zu nutzen.

## Ausblick

Sobald man komplexere Typen schreibt, die über einfache Literal Types, Union Types oder Intersection Types (in diesem Artikel nicht behandelt) hinausgehen, kann es sinnvoll sein, auch die Typen durch eigene Unittests abzudecken.
Wie man das tun kann, zeige ich in der Fortsetzung.

## Bonus für "Mögliche Altersbeschränkungen erhalten"

In der oben angegebenen Implementierung
```typescript
function getPossibleAgeRatings(): AgeRating[] {
    return [0, 6, 12, 16, 18]
}
```
ist es weiterhin möglich, die fehlerhaften Werte `[0, 6, 18]` oder `[0, 6, 6, 12, 16, 18]` zurückzugeben. Aber auch das können wir durch neue Typen optimieren:

```typescript
export type AllCombinationsOf<T, U = T> = [T] extends [never]
    ? []
    : T extends U
        ? [T, ...AllCombinationsOf<Exclude<U, T>>]
        : never
```

Der Typ `AllCombinationsOf<T>`, auf den am Ende alles hinausläuft, nimmt einen Union Type und lässt sämtliche Tupel zu, in denen jeder Wert aus der Union genau einmal vorkommt.
Damit sind die Werte `[0, 6, 12, 16]` (`18` fehlt) oder `[0, 6, 6, 12, 16, 18]` (`6` ist doppelt) nicht dem Typen `AllCombinationsOf<AgeRating>` zuweisbar, `[0, 6, 12, 16, 18]` oder zum Beispiel `[12, 18, 0, 16, 6]` aber weiterhin.

Verwenden wir nun diesen Typen als Rückgabewert der Funktion, so wirft Typescript auch immer dann einen Fehler, wenn man einen Wert im Array vergessen oder doppelt hat und wir haben eine weitere Sicherheit.

```typescript
function getPossibleAgeRatings(): AllCombinationsOf<AgeRating> {
  return [0, 6, 12, 16, 18]
}
```

Spätestens an dieser Stelle sollte der Vorteil, der sich durch strengere Typen ergibt, klar sein.
Ob das den zusätzlichen Aufwand zum Schreiben und Testen der Typen wieder wettmacht, muss von jedem selbst beantwortet werden.
Für mich lautet die Antwort: Ja! Außerdem macht es echt Spaß, sich zu überlegen, wie man einen gewünschten Typen definieren muss, damit er am Ende genau das tut, was man gerne hätte ;-)