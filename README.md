# Extremely strict Types in TypeScript - What is possible and how practical is it

## What to expect

TypeScript's type system is very versatile. It provides many options to define types, from simple union types to complex
recursive generic types. But how far can you go with it? Can you create types that are as strict as possible? What are
the benefits we gain from this, if any?

In this short XXXXX we will define types for a given scenario and test out the limitations and benefits of this.

## Disclaimer

Since the type system of TypeScript exists only until compile time, we need to ensure the correct typing by code. This
means, if we have a function `someFunc: () => 1 | 2` that returns (by type definition) either `1` or `2`, the code needs
to make sure that it really only returns `1` or `2`. If it would, for example, take some user input, parse it as number
and then return it, it could happen that the returned value is neither `1` nor `2`. And since all types are erased once
the code is executed, no exception would be thrown (like in other, strongly typed languages like Java or C#). Thus, we'd
make sure to throw an exception in this case by our own validation.

### Example

**Bad:**

```typescript
function getInput(): 1 | 2 {
    return someNumberInputField.value
}
```

**Good:**

```typescript
function getInput(): 1 | 2 {
    const parsedNumber = someNumberInputField.value
    if (parsedNumber !== 1 || parsedNumber !== 2) {
        throw new Error('Input is neither 1 nor 2')
    }
    return parsedNumber
}
```

However, writing these validations should be straight forward (even though for our advanced types, it could be a bit
more complex) so we will omit this in the further examples. Once you validated all values that are not generated inside
the code, you can rely on you typings to keep this consistent (as long as your code is correct, of course).

## Scenario

- We have a library that we want to administer.
- The library has an inventory of books and movies.
    - Books have a name, id, author and section.
        - The section consists of the first two letters of the author in upper case. We assume authors' names always
          have at least two digits.
        - Ids for books start with the letter 'B', followed by a dash, the section, a dash and a number
    - Movies have a name, id and ageRating (0, 6, 12, 16, 18).
        - Ids for movies start with the letter 'M', followed by a dash, the age Rating, a dash and a number

## The types

### Initial definitions

First, let's create the types without even thinking about it:

```typescript
type Book = {
    name: string
    id: string
    author: string
    section: string
}

type Movie = {
    name: string
    id: string
    ageRating: number
}
```

Ok, it's very obvious that we can create a common super class for these two elements:

```typescript
type Media = {
    name: string
    id: string
}

type Book = Media & {
    author: string
    section: string
}

type Movie = Media & {
    ageRating: number
}
```

### ageRating

That's it, our types are defined. Or could we specify the types even more, using what we know about the possible values?
Let's start with ageRating. We know, that the only possible values are 0, 6, 12, 16 and 18. But currently, the type
allows all possible number. We could create a movie with ageRating 5. To prohibit this, we can use union types to better
represent the type.

```typescript
type Movie = Media & {
    ageRating: 0 | 6 | 12 | 16 | 18
}
```

That looks better. Hm, but let's extract the ageRating as a separate type to also give meaning to it:

```typescript
type AgeRating = 0 | 6 | 12 | 16 | 18

type Movie = Media & {
    ageRating: AgeRating
}
```

### id

Now let's get to the id. We know the structure of the id for movies and books. So let's restrict the type to better
represent that structure. Currently, the id is part of the super class. We should leave it there but make it more
specific in the children. To specify the type, we can use template literal types:

```typescript
type Book = {
    author: string
    section: string
    id: `B-${string}-${number}`
}

type Movie = {
    ageRating: AgeRating
    id: `M-${AgeRating}-${number}`
}
```

Looks much better now. If we try to set the id of a book `book` to some non-matching string `book.id = '12345'`,
typescript will mark this code and fail while compiling. On the other hand, `book.id = 'B-SomeSecion-12345` works quite
nicely. For our movie we even used the `AgeRating` type from above to further restrict the types. Possible values are:

```typescript
movie.id = 'M-0-12345'
movie.id = 'M-18-5'
```

Impossible values are:

```typescript
movie.id = '0-12345' // The `M-` at the beginning is missing
movie.id = 'M-12345' // The `-${AgeRating}` part is missing
movie.id = 'M-5-12345' // 5 is no possible value for AgeRating
movie.id = 'M-6-12xy5' // 12xy5 is not of type number
```

Since we might need these id types somewhere else (e.g. as return value of some generator function), lets extract them.

```typescript
type BookId = `B-${string}-${number}`
type MovieId = `M-${AgeRating}-${number}`
```

Written right underneath each other, we see another similarity that we could extract: Both ids start with a prefix
specific to the media type. So lets extract this as separate types as well and change our id for Media as well:

```typescript
type BookType = 'B'
type MovieType = 'M'
type MediaType = BookType | MovieType
type MediaId = `${MediaType}-${string}-${number}`
type BookId = `${BookType}-${string}-${number}`
type MovieId = `${MovieType}-${AgeRating}-${number}`

type Media = {
    id: MediaId
    name: string
}

type Book = Media & {
    id: BookId
    author: string
    section: string
}

type Movie = Media & {
    id: MovieId
    ageRating: AgeRating
}

```

### section

Now let's have a look at the section of books. Could we narrow it down a bit? We know that it consists only of upper
case letters. Typescript provides the Uppercase<T> utility type that seems perfect for this. However, it only works on
string literal types and `Uppercase<string>` still allows for lower case characters in the result. So we need a
workaround. We could create a `SingleLetter` type `type SingleLetter = 'a' | 'b' | 'c' | ...` that is the union type of
all single letters that could occur in an authors name and then
use ``type Section = Uppercase<`${SingleLetter}${SingleLetter}`>``
Or we could make our Book type generic and make it dependent on the author (`type Book<AUTHOR extends string>`). Let's
try both approaches and discuss each benefits and disadvantages:

#### SingleLetter

Writing `'a' | 'b' | 'c' | ...` is quite annoying, especially if you want to include accented letters as well. It would
be nice if at least we could specify the possible values in a single string, such as `'abcdef...'`. So let's write a
generic helper type for this:

```typescript
type CharacterOf<CHARACTERS extends string> = CHARACTERS extends `${infer HEAD}${infer TAIL}`
    ? HEAD | CharacterOf<TAIL>
    : never
```

Ok, what does this do? Let's break it down to understand what we are doing:

```
type CharacterOf<CHARACTERS extends string>
```

We define a generic type that takes some string type as first parameter `CHARACTERS`.

```
CHARACTERS extends `${infer HEAD}${infer TAIL}`
```

First, we check whether the given parameter `CHARACTERS` can be represented as the combination of two inferred
types `HEAD` and `TAIL`. If `CHARACTERS` is just `string`, this evaluates to false. If `CHARACTERS` is a template
string, however, it becomes interesting:
If `CHARACTERS` is the empty template string `''`, it evaluates to false since `''` can only be represented
as `` `${''}` `` (in theory, it could be infinite empty strings behind each other but this is not how it works ;-)).
If `CHARACTERS` is any other template string, say `'abcde'`, this could also be represented as `` `${'a'}${'bcde'}` `` (
typescript only matches the first part as far as needed). So it evaluates to true and we can, in this case, make use of
the inferred types `HEAD` and `TAIL`. Note: A single character string `'a'` ist matched to `` `${'a'}${''}` ``.

```
? HEAD | CharacterOf<TAIL>
```

So if the type restriction above evaluated to true, we now have the first character of `CHARACTERS` in `HEAD` and the
remaining characters in `TAIL`. All possible characters can be described as the possible characters of `TAIL` and
additionally the type `HEAD`: a prime example of recursion. So we return `HEAD | CharacterOf<TAIL>`.

```
: never
```

If the type restriction above evaluated to false, we return `never`. Let's think about the cases in which it evaluates
to false:
`string` and the empty string type `''`. For `''` never is perfectly fine. In the end, we want a type to represent all
characters that occur in the string `CHARACTERS` and if there are no characters in the string, the type should not allow
any values. For the case `string` it would be nice to actually allow all possible characters instead of none. However,
this is not needed in our case so we just ignore it.

A recursion always needs a terminal condition, so let's check this briefly. Each iteration, `TAIL` gets one character
shorter. If, eventually, `TAIL` becomes the empty string, we return `never` and the recursion stops.

So what would the implementation using this type look like? (If you want to allow accented characters as well, just add
them to the string type in line 4).

```typescript
type CharachterOf<T extends string> = T extends `${infer HEAD}${infer TAIL}`
    ? HEAD | CharachterOf<TAIL>
    : never
type SingleLetter = CharachterOf<'abcdefghijklmnopqrstuvwxyz'>
type Section = Uppercase<`${SingleLetter}${SingleLetter}`>
type BookId = `${BookType}-${Section}-${number}`

type Book = Media & {
    id: BookId
    author: string
    section: Section
}
```

#### Generic Book

The second solution we want to investigate is to make Book a generic Type that has the Author as literal type as
parameter:

```typescript
type Book<AUTHOR extends string> = Media & {
    id: BookId
    author: AUTHOR
    section: string
}
```

Now we can use `AUTHOR` to further restrict the section type. For this we want a generic `Section` type that returns the
section of a given author:

```typescript
type Section<AUTHOR extends string> = AUTHOR extends `${infer FIRST}${infer SECOND}${infer TAIL}`
    ? Uppercase<`${FIRST}${SECOND}`>
    : string
```

If `AUTHOR` consists of at least two characters, we take these two and return the Uppercase of them. Otherwise, we just
use the type `string`. (We could also use `never`, but then `Section<string>` would be `never`; however, `string` as
fallback in this case makes more sense).
any
In combination and with also modifying the BookId Type:

```typescript
type Section<AUTHOR extends string> = AUTHOR extends `${infer FIRST}${infer SECOND}${infer TAIL}`
    ? Uppercase<`${FIRST}${SECOND}`>
    : string
type BookId<AUTHOR extends string> = `${BookType}-${SectionByAuthor<AUTHOR>}-${number}`


type Book<AUTHOR extends string> = Media & {
    id: BookId<AUTHOR>
    author: AUTHOR
    section: Section<AUTHOR>
}
```

#### Comparison

We have seen two possible (but not exclusive) solutions to further restrict the section type. Now let's compare them:

**Solution with SingleLetter**

*Advantages*

- Section is still quite fuzzy. This means we can use it on other places as well, for example if we would sort our
  movies in sections as well.
- If we decided to change the section algorithm for books to, let's say, first and last letter of name, we could still
  use our type without modifications.

*Disadvantages*

- It's still possible to have a book `{author: 'Twain, Mark', section: 'AB', id: 'B-XY-12345', ...}` that is valid in
  our type system.
- It can be cumbersome to specify all possible letters that can occur.
- If the author's name contains a letter that was not specified in our string, it's impossible to create a type valid
  entry for the author's books.

**Solution with generics**

*Advantages*

- It's as strictly typed as possible. It's **not** possible to have a type valid
  book `{author: 'Twain, Mark', section: 'AB', id: 'B-XY-12345', ...}`

*Disadvantages*

- When specifying the type for a book instance, we always need to have the author typed as well. In most cases, this is
  not the case and there is no workaround for it. For example, a method that fetches a book by a HttpRequest can never
  return `Book<SomethingSpecific>` but always only `Book<string>`.
- When we just want a book and don't care about the specific author, so we just use `Book<string>`, there is no type
  restriction on `section` at all (apart from just `string`)

#### Conclusion

In almost all applications the data that is processed comes from an external source (database, user input, ...). Thus,
while the solution with generics looks nice on paper, it's not practical for daily use. It only provides all benefits in
applications that create all data themselves which is negligible. The solution with `SingleLetter` is on paper not as
powerful, provides in real life much more safety, however.

So in the next part we will go with the `SingleLetter` solution.

## Why should we do this?

In the following we will lie out some functions that utilise our created types to manipulate books and movies safely. We
will discuss the difference between our strict types and the initial types that only utilised `string` and `number`.
Usually, the actual implementation is identical, the only differences can be found in the typings.

### Getting the section for an author

**Strict**

```typescript
function getSectionForAuthor(author: string): Section {
    return author.substring(0, 2).toUpperCase() as Section
}
```

**Non-strict**

```typescript
function getSectionForAuthor(author: string): string {
    return author.substring(0, 2).toUpperCase()
}
```

While in the strict solution we have to explicitly cast to Section, we do not have to do any casting in the non-strict
case. If you stick to the rule that you should only cast if you are 100% sure that there can be no problem here, the
strict case reminds you to also check for capitalization. So even if it does not directly help to find errors, it forces
you to think about your code more.

### Getting possible age ratings

If, for example, you provide an interface for your library personnel to edit movies and there is a dropdown menu for the
age rating, you need to provide all possible options for this dropdown.

**Stict**

```typescript
function getPossibleAgeRatings(): AgeRating[] {
    return [0, 6, 12, 16, 18]
}
```

**Non-stict**

```typescript
function getPossibleAgeRatings() {
    return [0, 6, 12, 16, 18]
}
```

In the strict solution, if we'd add `15` to the array, we'd get a compiler error because `15` is not included in
type `AgeRating`. In the non-strict case, this error would slip through.

Also, in the strict typed variant we can make use of the IDE auto suggestions. It already knows what values are possible
in the returned array and only proposes these as first suggestions.

### Get all books for a section

We want a function that consumes a section given in lower case letters, and we want to return all books that belong to
this section. Ok, admittedly this example is a bit constructed with the specification of lower case letter, but for the
sake of it, let's look at it.

Assume, we already have a function

```typescript
declare function getAllBooks(): Book[]
```

**Strict**

```typescript
function getBooksForSection(section: `${SingleLetter}${SingleLetter}`): Book[] {
    return getAllBooks().filter(book => book.section === section.toUpperCase())
}
```

**Non-strict**

```typescript
function getBooksForSection(section: `${SingleLetter}${SingleLetter}`): Book[] {
    return getAllBooks().filter(book => book.section === section.toUpperCase())
}
```

If we forgot the `.toUpperCase()` part, the strict implementation would give a compile time error since the expression
would never evaluate to true.

### Create a new book

Given the name and author, we want to create a new book in our library. For the last part of the id generation, let's
say we already have a generator function

```typescript
declare function getNextNumber(): number
```

**Strict**

```typescript
function createBook(name: string, author: string): Book {
    const section = getSectionForAuthor(author)
    return {
        name,
        author,
        section,
        id: `B-${section}-${getNextNumber()}` as const
    }
}
```

**Non-strict**

```typescript
function createBook(name: string, author: string): Book {
    const section = getSectionForAuthor(author)
    return {
        name,
        author,
        section,
        id: `B-${section}-${getNextNumber()}`
    }
}
```

Note that we need to use `as const` in the strictly types version to instruct typescript to infer the most restrictive
type for this value.

In the strict version, if we typed a `V` or even a `b` for the id, this would lead to a compile time error. In the
non-strict variant, we could use any string as id, it would not even have to stick to the general structure with two
dashes.

## Conclusion

The examples above show that there are some benefits of using the strict types when it comes to safety and usability.
Most of the problematic cases could also have been easily secured by unit tests so that the benefits are not that significant.
However, in my opinion, the code itself is still more precise, does not leave that much space for errors and the additional work that needs to be done
to build a stricter type system initially is well invested.