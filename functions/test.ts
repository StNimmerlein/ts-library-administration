import {
    AgeRating, AllCombinationsOf,
    Book,
    BookForAuthor,
    BookId,
    Media,
    Movie,
    MovieId,
    Section,
    SectionForAuthor, SingleLetter,
} from '../types/media'

function isBook(media: Media): media is Book {
    return media.id.startsWith('B')
}
function isMovie(media: Media): media is Movie {
    return media.id.startsWith('M')
}

function sortMedias(medias: Media[]) {
    // the syntax books = medias.filter(media => isBook(media) is not supported by typescript type guards (yet)
    const books: Book[] = medias.filter(isBook)
    const movies: Movie[] = medias.filter(isMovie)
    return {books, movies}
}

function getSectionForAuthor(author: string): Section {
    return author.substring(0, 2).toUpperCase() as Section
}

function getPossibleAgeRatings(): AllCombinationsOf<AgeRating> {
    return [0, 6, 12, 16, 18]
}

function validateId(media: Media): boolean {
    if (isBook(media)) {
        return /B-[A-Z]{2}-{0-9}*/.test(media.id) && media.id.substring(3, 5) === media.section
    }
    if (isMovie(media)) {
        return /M-(0|6|12|16|18)-{0-9}*/.test(media.id) && media.id.split('-')[1] === `${media.ageRating}`
    }
    return /[MB]-.*/.test(media.id)
}

declare function getAllBooks(): Book[]

function getBooksForSection(section: `${SingleLetter}${SingleLetter}`): Book[] {
    return getAllBooks().filter(book => book.section === section.toUpperCase())
}

declare function getNextNumber(): number

function createBook(name: string, author: string): Book {
    const section = getSectionForAuthor(author)
    return {
        name,
        author,
        section,
        id: `B-${section}-${getNextNumber()}` as const
    }
}