export type AgeRating = 0 | 6 | 12 | 16 | 18
export type BookType = 'B'
export type MovieType = 'M'
export type BookId = `${BookType}-${Section}-${number}`
export type BookIdForAuthor<AUTHOR extends string> = `${BookType}-${SectionForAuthor<AUTHOR>}-${number}`
export type MovieId = `${MovieType}-${AgeRating}-${number}`
export type CharachterOf<T extends string> = T extends `${infer HEAD}${infer TAIL}`
        ? HEAD | CharachterOf<TAIL>
        : never
export type MediaId = BookId | MovieId
export type SingleLetter = CharachterOf<'abcdefghijklmnopqrstuvwxyz'>
export type Section = Uppercase<`${SingleLetter}${SingleLetter}`>
export type SectionForAuthor<AUTHOR extends string> = AUTHOR extends `${infer FIRST}${infer SECOND}${infer TAIL}`
    ? Uppercase<`${FIRST}${SECOND}`>
    : never
// export type UniqueTupleHelper<T extends string | number> =
//     {[k in T]: Exclude<T, k> extends never ? undefined : UniqueTupleHelper<Exclude<T, k>>}
// export type Flatten<T> = {} extends T
//     ? []
//     : {[k in keyof T]: [k, ...Flatten<Omit<T, k>>]}[keyof T]
// export type AllCombinationsOf<T extends string | number> = Flatten<UniqueTupleHelper<T>>

export type AllCombinationsOf<T, U = T> = [T] extends [never]
    ? []
    : T extends U
        ? [T, ...AllCombinationsOf<Exclude<U, T>>]
        : never

export type Media  = {
    id: MediaId
    name: string
}

export type Book = Media & {
    id: BookId
    author: string
    section: Section
}

export type BookForAuthor<AUTHOR extends string> = Media & {
    id: BookIdForAuthor<AUTHOR>
    author: AUTHOR
    section: SectionForAuthor<AUTHOR>
}

export type Movie = Media & {
    id: MovieId
    ageRating: AgeRating
}